package com.internship.QuestionAPI.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.internship.QuestionAPI.domain.Topics;

@Service
public class QuestionService {

	List<Topics> topics = new ArrayList<>();

	QuestionService(List<Topics> topics) {
		this.topics = topics;
		Topics a1 = new Topics();
		a1.setId(1);
		a1.setName("Java");
		a1.setDetail("Most used Programming Language");

		Topics a2 = new Topics();
		a2.setId(2);
		a2.setName("Cloud");
		a2.setDetail("Very important Technology");

		topics.add(a1);
		topics.add(a2);
	}

	public List<Topics> getQuestionTopics() {
		return topics;
	}

	public boolean checkifTopicExsists(String name) {
		for (Topics namepresent : topics) {
			if (namepresent.getName().equals(name)) {
				return true;
			}
		}
		return false;

	}

	public List<Topics> createQuestionTopic(String name, String detail) {
        int count = topics.size();
		Topics newtopic = new Topics();
		newtopic.setId(count+1);
		newtopic.setName(name);
		newtopic.setDetail(detail);

		topics.add(newtopic);
		return topics;
	}

	public void deleteQuestionTopics(String name) {
		for (Topics namepresent : topics) {
			if (namepresent.getName().equals(name)) {
				topics.remove(namepresent);
			}
		}
	}

	public void updateQuestionTopics(String name, String detail) {
		for (Topics topic : topics) {
			if (topic.getName().equals(name)) {
				topic.setDetail(detail);
			}
		}
		
	}
}

