package com.internship.QuestionAPI.controllers;

import com.internship.QuestionAPI.domain.Topics;
import com.internship.QuestionAPI.services.QuestionService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class QuestionController {

	@Autowired
	QuestionService topicService;

	@GetMapping("topics")  
	public List<Topics> topicsRead() {
		return topicService.getQuestionTopics();
		}

    @PostMapping("topics")
	public List<Topics> topicCreate(@RequestBody Topics topics) { 
	    if(topicService.checkifTopicExsists(topics.getName())) {
	     throw new ResponseStatusException(HttpStatus.CONFLICT,"Duplicate Topic Found");	}
	    else
    	return topicService.createQuestionTopic(topics.getName(),topics.getDetail());
	 }
	 
	@DeleteMapping(value = "topics/{topicName}")
	public void topicsDelete(@PathVariable("topicName") String name) {
	    topicService.deleteQuestionTopics(name);
	  }
	
	@PutMapping(value = "topics/{topicName}")
    public void update(@PathVariable("topicName") String name, @RequestBody Topics topics) {
		 if(topicService.checkifTopicExsists(topics.getName())) {
	     topicService.updateQuestionTopics(topics.getName(),topics.getDetail());
	}
	}
}
